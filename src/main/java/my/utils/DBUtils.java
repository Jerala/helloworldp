package my.utils;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

public class DBUtils {

    public static Connection getConnection() throws SQLException {
        //return DriverManager.getConnection("jdbc:oracle:thin:@localhost:1521/XE", "SYSTEM", "jerala");
        return DriverManager.getConnection("jdbc:postgresql://localhost:5432/snippethub",
                "postgres", "123");
    }

}
