package my.dao;

import java.sql.*;
import java.util.*;

import javax.sql.DataSource;

import my.entity.Users;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.jdbc.core.support.JdbcDaoSupport;
import org.springframework.transaction.annotation.Transactional;
import my.utils.DBUtils;

@Repository
@Transactional
public class UsersDAO extends JdbcDaoSupport {

    private static final Map<Long, Users> USERS_MAP = new HashMap<>();

    static {
        initDATA();
    }

    private static void initDATA() {

        try {
            Connection con = DBUtils.getConnection();
            PreparedStatement stmt = con.prepareStatement("SELECT * FROM public.USERS");
            ResultSet rs = stmt.executeQuery();

            while (rs.next()) {
                Users user = new Users();
                user.setUserId(rs.getLong("USER_ID"));
                user.setUserName(rs.getString("USER_NAME"));
                user.setEmail(rs.getString("EMAIL"));
                user.setEncrytedPassword(rs.getString("PASSWORD"));
                user.setEnabled(false);
                USERS_MAP.put(user.getUserId(), user);
            }

            con.close();
            stmt.close();
        } catch (SQLException e) {
            System.out.println(e);
        }
    }

    public static Map<Long, Users> getUsersMap() {
        return USERS_MAP;
    }

    @Autowired
    public UsersDAO(DataSource dataSource) {
        this.setDataSource(dataSource);
    }

    public static Long getMaxUserId() {
        long max = 0;
        for (Long id : UsersDAO.getUsersMap().keySet()) {
            if (id > max) {
                max = id;
            }
        }
        return max;
    }
}