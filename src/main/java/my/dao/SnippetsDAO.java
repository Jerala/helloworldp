package my.dao;

import my.utils.DBUtils;

import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.Types;

public class SnippetsDAO {

    public static int getMaxSnippetId() {
        try {
            Connection connection = DBUtils.getConnection();
            CallableStatement stmt = connection.prepareCall("{? = call getmaxsnippetid()}");
            stmt.registerOutParameter(1, Types.INTEGER);
            stmt.execute();
            return stmt.getInt(1);
        }
        catch(Exception e) {
            System.out.println(e);
        }
        return -1;
    }
}
