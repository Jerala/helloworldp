package my.controllers;

import my.entity.Artist;
import my.entity.Users;
import my.services.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import java.util.List;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

@Controller
public class MainController {

    private UsersFinderService usersFinderService;

    private ArtistFinderService artistFinderService;

    @Autowired
    public MainController(UsersFinderService usersFinderService, ArtistFinderService artistFinderService) {
        this.artistFinderService = artistFinderService;
        this.usersFinderService = usersFinderService;
    }


    @RequestMapping("/")
    public String viewHome(Model model) {
        return "welcomePage";
    }

//    @RequestMapping(value = "/users", method = RequestMethod.GET)
//    @ResponseBody
//    public List<Users> showUsers(@RequestParam(value="user", required = false,
//            defaultValue = ".") String name) {
//
//        if(!name.equals("."))
//            return usersFinderService.findUserByName(name);
//        else
//            return usersFinderService.findAllUsers();
//
//    }

    @RequestMapping(value = "/artists", method = RequestMethod.GET)
    @ResponseBody
    public List<Artist> showArtist(@RequestParam(value="artist", required = false,
    defaultValue = "909253") String id) {
        return artistFinderService.findArtistById(id);
    }

    @RequestMapping(value = "/artist")
    public String showArtists(@RequestBody String id) {
        return id;
    }
}