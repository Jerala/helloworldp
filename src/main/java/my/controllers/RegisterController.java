package my.controllers;

import my.entity.Users;
import my.formbean.UserForm;
import my.services.UserCreatorService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

import static my.utils.AuthChecker.isAuth;

@Controller
public class RegisterController {

    @Autowired
    private UserCreatorService userCreatorService;

    @Autowired
    public RegisterController(UserCreatorService userCreatorService)
    {
        this.userCreatorService = userCreatorService;
    }

    @RequestMapping(value = "/register", method = RequestMethod.GET)
    public String viewRegister(Model model) {

        UserForm form = new UserForm();

        model.addAttribute("appUserForm", form);

        return "registerPage";
    }

    @RequestMapping(value = "/register", method = RequestMethod.POST)
    @ResponseBody
    public Users saveRegister(Model model, //
                               @ModelAttribute("appUserForm") UserForm appUserForm) {

        return userCreatorService.createNewUser(appUserForm);

    }
}
