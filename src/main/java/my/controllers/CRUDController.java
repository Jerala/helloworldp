package my.controllers;

import my.entity.Snippets;
import my.entity.Users;
import my.services.SnippetsService;
import my.services.UsersService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@CrossOrigin
public class CRUDController {

    @Autowired
    private UsersService usersService;

    @Autowired
    private SnippetsService snippetsService;

    @RequestMapping(value = "/users/findById", method = RequestMethod.GET)
    public Users getUserById(@RequestParam Long id) {
        return usersService.getUserById(id);
    }

    @RequestMapping(value = "/users", method = RequestMethod.GET)
    public List<Users> getUsers() {
        return usersService.getAllUsers();
    }

    @RequestMapping(value = "/users/findByName", method = RequestMethod.GET)
    public Users getUsers(@RequestParam String name) {
        try {
            return usersService.getUserByName(name).get(0);
        }
        catch(IndexOutOfBoundsException e) { return null; }
    }

    @RequestMapping(value = "/users/delete", method = RequestMethod.GET)
    public String removeUserById(@RequestParam Long id) {
        usersService.deleteUser(id);
        return usersService.getUserById(id) == null ? "OK" : "NOT OK";
    }

    @RequestMapping(value = "/users/add", method = RequestMethod.GET)
    public String getUsers(@RequestParam String user_name,
                           String email, String password) {
        usersService.saveUser(new Users(user_name, email, password));
        return "OK";
    }

    // snippets

    @RequestMapping(value = "/snippets/findById", method = RequestMethod.GET)
    public Snippets getSnippetById(@RequestParam Long id) {
        return snippetsService.getSnippetById(id);
    }

    @RequestMapping(value = "/snippets", method = RequestMethod.GET)
    public List<Snippets> getSnippets() {
        return snippetsService.getAllSnippets();
    }

    @RequestMapping(value = "/snippets/deleteById", method = RequestMethod.GET)
    public String deleteSnippetById(@RequestParam Long id) {
        snippetsService.deleteSnippet(id);
        return snippetsService.getSnippetById(id) == null ? "OK" : "NOT OK";
    }

    @RequestMapping(value = "/snippets/findByUserId", method = RequestMethod.GET)
    public List<Snippets> findSnippetsByUserId(@RequestParam Long id) {
        return snippetsService.getSnippetsByUserID(id);
    }

    @RequestMapping(value = "/snippets/findByName", method = RequestMethod.GET)
    public List<Snippets> getSnippetById(@RequestParam String name) {
        return snippetsService.getSnippetByName(name);
    }

    @RequestMapping(value = "/snippets/add", method = RequestMethod.GET)
    public String getSnippetById(@RequestParam Long pl_id,
                                 String snippet_name,
                                   String user_name) {
        Users user = null;
        try {
            user = usersService.getUserByName(user_name).get(0);
        }
        catch(IndexOutOfBoundsException e) { return "No user with such name."; }
        snippetsService.saveSnippet(new Snippets(pl_id, snippet_name, user));
        return "OK";
    }

}
