package my.repositories;

import my.entity.Snippets;
import org.springframework.data.repository.CrudRepository;

public interface SnippetsRepository extends CrudRepository<Snippets, Long> {
}
