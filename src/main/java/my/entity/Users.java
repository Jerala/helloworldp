package my.entity;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.Data;
import my.dao.UsersDAO;
import javax.persistence.*;
import java.util.List;

@Entity
@Table(name = "users")
@Data
public class Users  {

    public Users() {}
    public Users(String name) {

    }
    public Users(Long userId, String userName, //
                 String email, String encrytedPassword, boolean enabled) {
        super();
        this.userId = userId;
        this.userName = userName;
        this.enabled = enabled;
        this.email = email;
        this.encrytedPassword = encrytedPassword;
        this.role_id = 2L;
    }

    public Users(String userName, String email, String password) {
        this.userId = UsersDAO.getMaxUserId() + 1;
        this.userName = userName;
        this.enabled = false;
        this.email = email;
        this.encrytedPassword = password;
        this.role_id = 2L;
    }

    @Id
    @Column(name = "user_id", nullable = false)
    private Long userId;

    @Column(name = "user_name", nullable = false)
    private String userName;

    @Column(name = "password", nullable = false)
    private String encrytedPassword;

    @Column(name = "enabled", nullable = false)
    private boolean enabled;

    @Column(name = "email", nullable = false)
    private String email;

    @Column(name = "role_id", nullable = false)
    private Long role_id;

    @JsonIgnore
    @OneToMany(mappedBy = "user", fetch = FetchType.LAZY)
    private List<Snippets> snippets;

}