package my.entity;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class Artist {

    private String wrapperType;

    private String artistType;

    private String artistName;

    private String artistLinkUrl;

    private String artistId;

    private String amgArtistId;

    private String primaryGenreName;

    private String primaryGenreId;

}
