package my.entity;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import my.dao.SnippetsDAO;

import javax.persistence.*;
import java.util.Date;

@Entity
@Table(name = "Snippets", //
        uniqueConstraints = { //
                @UniqueConstraint(name = "Snippets_UK", columnNames = "Snippet_id") })
@Data
@AllArgsConstructor
@NoArgsConstructor
public class Snippets{

    public Snippets(Long PL_ID, String snippet_name,
                    Users user) {
        this.snippet_id = (long) SnippetsDAO.getMaxSnippetId() + 1;
        this.PL_ID = PL_ID;
        this.snippet_name = snippet_name;
        this.user = user;
        this.upload_date = new Date();
        this.like_count = 0L;
        this.approved = 0L;
        this.tags = "";
    }

    @Id
    @Column(name = "Snippet_Id", nullable = false)
    private Long snippet_id;

    @Column(name = "PL_ID", length = 5, nullable = false)
    private Long PL_ID;

    @Column(name = "Snippet_name", length = 100, nullable = false)
    private String snippet_name;

    @JsonIgnore
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "user_id")
    private Users user;

    @Column(name = "UPLOAD_DATE", nullable = false)
    private Date upload_date;

    @Column(name = "LIKE_COUNT", length = 10, nullable = false)
    private Long like_count;

    @Column(name = "approved", length = 10, nullable = false)
    private Long approved;

    @Column(name = "TAGS", nullable = true)
    private String tags;

}
