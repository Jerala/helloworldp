package my.services;

import my.entity.Users;
import my.formbean.UserForm;
import org.springframework.stereotype.Service;

@Service
public interface UserCreatorService {

    Users createNewUser(UserForm form);

    Users createNewUser(UserForm form, Long id, String password);

}
