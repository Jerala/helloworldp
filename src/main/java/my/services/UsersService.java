package my.services;

import my.entity.Users;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public interface UsersService {

    public Users saveUser(Users user);

    public void deleteUser(Long id);

    public Users getUserById(Long id);

    public List<Users> getAllUsers();

    public List<Users> getUserByName(String Name);

}
