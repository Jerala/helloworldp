package my.services.serviceImpl;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import my.entity.Artist;
import my.services.ArtistFinderService;
import org.springframework.http.*;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Service
public class ArtistFinderServiceImpl implements ArtistFinderService {
    public List<Artist> findArtistById(String id) {

        List<Artist> artistList = new ArrayList<>();
        try {
            JsonNode nodes = new ObjectMapper()
                    .readTree(new URL("https://itunes.apple.com/lookup?id=" + id)).get("results");
            ObjectMapper mapper = new ObjectMapper();
            for(final JsonNode node : nodes){
                Artist artist;
                String jsonString = node.toString();
                artist = mapper.readValue(jsonString, Artist.class);
                artistList.add(artist);
            }
        }
        catch(IOException e) { System.out.println(e); }
        return artistList;
    }
}
