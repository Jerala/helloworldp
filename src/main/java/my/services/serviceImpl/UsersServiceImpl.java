package my.services.serviceImpl;

import my.entity.Users;
import my.repositories.UsersRepository;
import my.services.UsersService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

@Service
public class UsersServiceImpl implements UsersService {

    private UsersRepository usersRepository;

    @Autowired
    public UsersServiceImpl(UsersRepository usersRepository) {
        this.usersRepository = usersRepository;
    }

    public Users saveUser(Users user) {
        return this.usersRepository.save(user);
    }

    @Override
    public void deleteUser(Long id) {
        this.usersRepository.deleteById(id);
    }

    public Users getUserById(Long id) {
        return getAllUsers().stream().filter(user -> user.getUserId().equals(id))
                .collect(Collectors.toList()).get(0);
    }

    public List<Users> getAllUsers() {
        List<Users> users = new ArrayList<>();
        this.usersRepository.findAll().forEach(users::add);
        return users;

    }

    public List<Users> getUserByName(String Name) {
        return getAllUsers().stream().filter(user -> user.getUserName().equals(Name))
                .collect(Collectors.toList());
    }
}
