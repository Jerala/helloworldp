package my.services.serviceImpl;

import my.dao.UsersDAO;
import my.entity.Users;
import my.services.UsersFinderService;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

@Service
public class UserFinderServiceImpl implements UsersFinderService {

    public List<Users> findUserByName(String userName) {
        Collection<Users> appUsers = UsersDAO.getUsersMap().values();
        List<Users> users = new ArrayList<>();
        for (Users u : appUsers) {
            if (u.getUserName().equals(userName)) {
                users.add(u);
            }
        }
        return users;
    }

    public List<Users> findAllUsers() {
        List<Users> list = new ArrayList<>();

        list.addAll(UsersDAO.getUsersMap().values());
        return list;
    }
}
