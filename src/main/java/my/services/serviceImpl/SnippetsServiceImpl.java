package my.services.serviceImpl;

import my.dao.SnippetsDAO;
import my.entity.Snippets;
import my.repositories.SnippetsRepository;
import my.services.SnippetsService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

@Service
public class SnippetsServiceImpl implements SnippetsService {

    private SnippetsRepository snippetsRepository;

    @Autowired
    public SnippetsServiceImpl(SnippetsRepository snippetsRepository) {
        this.snippetsRepository = snippetsRepository;
    }

    public Snippets saveSnippet(Snippets snippet) {
        return this.snippetsRepository.save(snippet);
    }

    public void deleteSnippet(Long id) {
      try {
          this.snippetsRepository.deleteById(id);
      }
      catch(Exception e) {
          System.out.println("Object doesnt exist");
      }
    }

    public Snippets getSnippetById(Long id) {
        return this.snippetsRepository.findById(id).orElse(null);
    }

    public List<Snippets> getAllSnippets() {
        List<Snippets> snippets = new ArrayList<>();
        this.snippetsRepository.findAll().forEach(snippets::add);
        return snippets;
    }

    public List<Snippets> getSnippetByName(String Name) {
        return getAllSnippets()
                .stream().filter(snippet -> snippet.getSnippet_name().equals(Name))
                .collect(Collectors.toList());
    }

    public List<Snippets> getSnippetsByUserID(Long userID) {
        return getAllSnippets().stream().filter(snippet -> snippet.getUser().getUserId().equals(userID))
                .collect(Collectors.toList());
    }
}
