package my.services.serviceImpl;

import my.dao.UsersDAO;
import my.entity.Users;
import my.formbean.UserForm;
import my.services.UserCreatorService;
import my.utils.DBUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;

@Service
public class UserCreatorServiceImpl implements UserCreatorService {

    @Autowired
    private PasswordEncoder passwordEncoder;

    public Users createNewUser(UserForm form) {
        return createNewUser(form, null, null);
    }

    public Users createNewUser(UserForm form, Long id, String password) {
        Long userId = null;
        String encrytedPassword;
        if(id == null || password == null) {
            userId = UsersDAO.getMaxUserId() + 1;
            encrytedPassword = this.passwordEncoder.encode(form.getPassword());
        }
        else {
            userId = id;
            encrytedPassword = password;
        }
        try
        {
            Connection con = DBUtils.getConnection();
            PreparedStatement stmt = con.prepareStatement("INSERT INTO public.USERS " +
                    "(USER_ID, USER_NAME, PASSWORD, EMAIL, ROLE_ID, ENABLED) VALUES (?, ?, ?, ?, ?, ?)");
            stmt.setLong(1, userId);
            stmt.setString(2, form.getUserName());
            stmt.setString(3, encrytedPassword);
            stmt.setString(4, form.getEmail());
            stmt.setInt(5, 2);
            stmt.setString(6, "0");

            stmt.executeUpdate();

            stmt.close();
            con.close();
        }
        catch(SQLException e) {
            System.out.println(e);
        }
        Users user = new Users(userId, form.getUserName(), //
                form.getEmail(),  //
                encrytedPassword, false);

        UsersDAO.getUsersMap().put(userId, user);
        return user;
    }


}
