package my.services;

import my.entity.Artist;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public interface ArtistFinderService {
    List<Artist> findArtistById(String id);
}
