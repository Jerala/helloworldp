package my.services;

import my.entity.Snippets;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service
public interface SnippetsService {

    public Snippets saveSnippet(Snippets snippet);

    public void deleteSnippet(Long id);

    public Snippets getSnippetById(Long id);

    public List<Snippets> getAllSnippets();

    public List<Snippets> getSnippetByName(String Name);

    public List<Snippets> getSnippetsByUserID(Long userID);
}
