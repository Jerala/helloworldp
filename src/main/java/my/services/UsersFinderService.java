package my.services;

import my.entity.Users;
import org.springframework.context.annotation.Bean;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public interface UsersFinderService {

    List<Users> findUserByName(String userName);

    List<Users> findAllUsers();

}
