package my;

import my.formbean.UserForm;
import my.services.UserCreatorService;
import my.services.serviceImpl.UserCreatorServiceImpl;
import my.utils.DBUtils;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Locale;

public class UsersCreatorTest {

    private UserCreatorService userCreatorService;

    private UserForm form;

    @Before
    public void fillForm() {
        form = new UserForm();
        form.setUserId(-1L);
        form.setUserName("test");
        form.setEmail("hello@edu.com");
        form.setPassword("123");
        userCreatorService = new UserCreatorServiceImpl();
    }

    @Test
    public void createUser() {
        Locale.setDefault(Locale.ENGLISH);
        userCreatorService.createNewUser(form, -1L, "123");
        try {
            Connection con = DBUtils.getConnection();
            PreparedStatement stmt = con.prepareStatement("SELECT * FROM USERS WHERE USER_ID=?");

            stmt.setLong(1, -1);

            ResultSet rs = stmt.executeQuery();
            Assert.assertEquals(rs.next(), true);

            stmt.close();
            con.close();
        } catch (SQLException e) {
            System.out.println(e);
        }

    }

    @After
    public void removeRecord() {
        try {
            Connection con = DBUtils.getConnection();
            PreparedStatement stmt = con.prepareStatement("DELETE FROM USERS WHERE USER_ID=?");
            stmt.setLong(1, form.getUserId());
            stmt.executeUpdate();
        }
        catch(Exception e) {System.out.println(e);}
    }

}
