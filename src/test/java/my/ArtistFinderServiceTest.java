package my;

import my.dao.UsersDAO;
import my.entity.Artist;
import my.entity.Users;
import my.services.ArtistFinderService;
import my.services.serviceImpl.ArtistFinderServiceImpl;
import my.services.serviceImpl.UserFinderServiceImpl;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import java.util.ArrayList;
import java.util.List;

public class ArtistFinderServiceTest {

    private ArtistFinderService artistFinderService;

    @Before
    public void init() {
        artistFinderService = new ArtistFinderServiceImpl();
    }

    @Test
    public void testFinderById() {

        List<Artist> artists = artistFinderService.findArtistById("909253");

        Assert.assertEquals(artists.get(0).getArtistName(), "Jack Johnson");
    }
}
